import { deepPurple } from '@material-ui/core/colors';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

export default createMuiTheme({
    palette: {
        type: 'dark',
        primary: deepPurple,
        secondary: deepPurple
    },
    typography: {
        useNextVariants: true
    }
});
