import GenericException from 'src/exceptions/GenericException';

/**
 * Exception thrown when the `HYDENOutputParser` is passed a file
 * that can't be parsed as a HYDEN output file,
 * and hence is 'invalid'.
 */
class InvalidHYDENFileException extends GenericException {

}

export default InvalidHYDENFileException;
