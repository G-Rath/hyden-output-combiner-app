import { Theme, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React, { useCallback, useContext } from 'react';
import Dropzone, { DropFilesEventHandler, DropzoneRenderArgs } from 'react-dropzone';
import { DragOverPageDetectorContext } from 'src/contexts/DragOverPageDetector';
import HYDENOutputParser, { ParsedHYDENOutputFile } from 'src/services/HYDENOutputParser';

export type HYDENFilesParsedEventHandler = (event: {
    parsed: Array<ParsedHYDENOutputFile>;
    failed: Array<File>;
}) => void;

interface Props {
    children?: never;
    className?: string;

    /**
     * Event handler for when files are parsed as HYDEN output files.
     */
    onHYDENFilesParsed: HYDENFilesParsedEventHandler;
}

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        borderStyle: 'solid',
        borderWidth: '1px',
        borderColor: '#666666',
        textAlign: 'center',
        cursor: 'pointer',
        padding: `${theme.spacing.unit * 5}px 0`,
        background: theme.palette.background.paper
    },
    dropzoneInput: {
        width: 0
    }
}));

/**
 * Checks if any of the types of the given `DataTransferItem`s that are
 * of kind `'file'`  are included in the given collection of mime-types.
 *
 * @param {ArrayLike<DataTransferItem>} items
 * @param {Array<string>} mimeTypes
 *
 * @return {boolean}
 */
const hasFilesOfMimeType = (items: ArrayLike<DataTransferItem>, mimeTypes: Array<string>): boolean => {
    for (let i = 0; i < items.length; i++) {
        const item = items[i];

        if (item.kind !== 'file') {
            continue;
        }

        if (mimeTypes.includes(item.type)) {
            return true;
        }
    }

    return false;
};

/**
 * A file dropzone component for `HYDEN` files.
 *
 * This component renders a {@link Dropzone} that accepts `HYDEN` output files.
 *
 * {@link DragOverPageDetectorContext} is used to react to dragging of items
 * over the entire page - if any of the items are files supported by this
 * component, then the component will react accordingly.
 *
 * When files are provided to the `Dropzone`, they're parsed using the
 * {@link HYDENOutputParser}, and then call the {@link Props#onHYDENFilesParsed} prop.
 */
const HYDENFileDropzone = (props: Props) => {
    const classes = useStyles(props);

    const dragOverPageContext = useContext(DragOverPageDetectorContext);

    /**
     * Handles when `onDrop` is called on the {@link Dropzone}, handling the dropped files.
     *
     * The files are passed to the {@link HYDENOutputParser} in an attempt to parse them.
     * The results are put into `parsed` & `failed` arrays, and dispatched via {@link Props#onHYDENFilesParsed}
     *
     * @type {DropFilesEventHandler}
     */
    const handleDropCallback = useCallback<DropFilesEventHandler>(async accepted => {
        const parsed: Array<ParsedHYDENOutputFile> = [];
        const failed: Array<File> = [];

        // map each file into a parsed promise
        await Promise.all(
            accepted.map(
                file => HYDENOutputParser.promiseParsedFile(file)
                                         .then(r => parsed.push(r))
                                         .catch(() => failed.push(file))
            )
        );

        props.onHYDENFilesParsed({ parsed, failed });
    }, [props.onHYDENFilesParsed]);

    const acceptedMimeTypes = [
        'text/plain'
    ];

    /**
     * Represents if this component should act
     * as if content is being dragged over it.
     *
     * @type {boolean}
     */
    const actAsIfDragging = dragOverPageContext.isDraggingOverPage && hasFilesOfMimeType(dragOverPageContext.dataTransferItems, acceptedMimeTypes);

    return (
        <Dropzone
            accept={acceptedMimeTypes.join(',')}

            onDrop={handleDropCallback}
        >
            {
                (
                    {
                        getRootProps,
                        getInputProps,
                        isDragActive
                    }: DropzoneRenderArgs
                ) => {
                    return (
                        <div
                            {...getRootProps()}
                            className={classes.root}
                        >
                            <input
                                {...getInputProps()}
                                className={classes.dropzoneInput}
                            />
                            <Typography>
                                {
                                    actAsIfDragging || isDragActive
                                        ? 'Drop your file here to be processed'
                                        : 'Click or drag files here'
                                }
                            </Typography>
                        </div>
                    );
                }
            }
        </Dropzone>
    );
};

HYDENFileDropzone.defaultProps = {
    className: undefined
};

export default HYDENFileDropzone;
