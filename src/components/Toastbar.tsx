import * as React from 'react';
import { useEffect, useState } from 'react';
import AppSnackbar from 'src/components/AppSnackbar';
import Toaster, { ToasterEvent, ToastShowEvent } from 'src/services/Toaster';

type Props = {}

const Toastbar = (props: Props) => {
    const [currentToast, setCurrentToastState] = useState<ToastShowEvent | null>(null);

    useEffect(() => {
        Toaster.on(ToasterEvent.E_TOAST_SHOW, setCurrentToastState);

        return () => Toaster.removeListener(ToasterEvent.E_TOAST_SHOW, setCurrentToastState);
    }, []);

    if (currentToast === null) {
        return null;
    }

    return (
        <AppSnackbar
            variant={currentToast.variant}
            messages={currentToast.messages}
        />
    );
};

export default Toastbar;
