import { createStyles, IconButton, Snackbar, Theme, Typography } from '@material-ui/core';
import { amber, lightBlue, lightGreen } from '@material-ui/core/colors';
import { Close } from '@material-ui/icons';
import { makeStyles } from '@material-ui/styles';
import React, { useCallback, useEffect, useRef, useState } from 'react';

export enum SnackbarVariant {
    SUCCESS = 'success',
    ERROR = 'error',
    INFO = 'info',
    WARNING = 'warning'
}

export interface QueuedSnackbar {
    variant: SnackbarVariant;
    messages: Array<string>;
    key: number;
}

const useStyles = makeStyles((theme: Theme) => createStyles({
    success: {
        backgroundColor: lightGreen['600']
    },
    error: {
        backgroundColor: theme.palette.error.dark
    },
    info: {
        backgroundColor: lightBlue['600']
    },
    warning: {
        backgroundColor: amber['700']
    },
    anchorOriginTopCenter: {
        top: theme.spacing.unit * 4
    }
}));

type Props = {
    messages: Array<string>;
    variant: SnackbarVariant;
}

/**
 *
 * @param {Array<string>} messages
 *
 * @return {React.ReactNode}
 */
const buildMessagesRenderContent = (messages: Array<string>): React.ReactNode => {
    if (messages.length === 0) {
        return null;
    }

    if (messages.length > 1) {
        return (
            <ul>
                {
                    messages.map(message => (
                            <Typography
                                key={btoa(message)}
                                component={'li'}
                            >
                                {message.trim()}
                            </Typography>
                        )
                    )
                }
            </ul>
        );
    }

    return (
        <Typography>
            {messages[0].trim()}
        </Typography>
    );
};

const AppSnackbar = (props: Props) => {
    const classes = useStyles(props);

    const [isOpen, setIsOpenState] = useState(false);
    const [messageInfo, setMessageInfoState] = useState<QueuedSnackbar | null>(null);

    const snackbarQueueRef = useRef<Array<QueuedSnackbar>>([]);

    const handleCloseCallback = useCallback((event: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setIsOpenState(false);
    }, [setIsOpenState]);

    const processQueueCallback = useCallback(() => {
        if (snackbarQueueRef.current.length) {
            setIsOpenState(true);
            setMessageInfoState(snackbarQueueRef.current.shift() || null);
        }
    }, [snackbarQueueRef, setIsOpenState, setMessageInfoState]);

    useEffect(() => {
        if (props.messages.length === 0) {
            return;
        }

        snackbarQueueRef.current.push({
            key: new Date().getTime(),
            messages: props.messages,
            variant: props.variant
        });

        isOpen // immediately dismiss current message & start showing new one
            ? setIsOpenState(true)
            : processQueueCallback();
    }, [props.messages]);

    if (messageInfo === null) {
        return null;
    }

    // todo: this can be memorized
    const formattedMessages = buildMessagesRenderContent(messageInfo.messages);

    return (
        <Snackbar
            key={messageInfo.key}
            classes={{ anchorOriginTopCenter: classes.anchorOriginTopCenter }}
            action={
                <IconButton
                    key="close"
                    aria-label="Close"
                    color="inherit"

                    onClick={handleCloseCallback}
                >
                    <Close />
                </IconButton>
            }
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'center'
            }}
            autoHideDuration={5000}
            ContentProps={{
                'aria-describedby': 'message-id',
                classes: { root: classes[messageInfo.variant] },
                // @ts-ignore - workaround for https://github.com/mui-org/material-ui/issues/13144
                headlineMapping: {
                    body1: 'div',
                    body2: 'div'
                }
            }}
            open={isOpen}
            message={<span id="message-id">{formattedMessages}</span>}

            onClose={handleCloseCallback}
            onExited={processQueueCallback}
        />
    );
};

export default AppSnackbar;
