import { Grid, Theme, Typography } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/styles';
import * as React from 'react';
import { useCallback, useState } from 'react';
import { SnackbarVariant } from 'src/components/AppSnackbar';
import HYDENFileDropzone, { HYDENFilesParsedEventHandler } from 'src/components/HYDENFileDropzone';
import { ParsedHYDENOutputFile } from 'src/services/HYDENOutputParser';
import { toast } from 'src/services/Toaster';

interface Props {
    children?: never;
}

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        margin: `${theme.spacing.unit * 2}px`,
        minWidth: '50%',
        textAlign: 'center'
    }
}));

const App = ({}: Props) => {
    const classes = useStyles({});
    const theme = useTheme<Theme>();

    const [hydenFiles, setHydenFilesState] = useState<Array<ParsedHYDENOutputFile>>([]);

    /**
     * Handles after files are attempted to be parsed as `HYDEN` files.
     *
     * @type {HYDENFilesParsedEventHandler}
     */
    const handleHYDENFilesParsedCallback = useCallback<HYDENFilesParsedEventHandler>(event => {
        if (event.failed.length) {
            toast(
                event.parsed.length
                    ? SnackbarVariant.WARNING
                    : SnackbarVariant.ERROR,
                'Some of the files could not be parsed. Make sure they\'re HYDEN output files!'
            );
        }

        setHydenFilesState(prevState => [...prevState, ...event.parsed]);
    }, [setHydenFilesState]);

    return (
        <Grid container justify={'center'}>
            <Grid item className={classes.root}>
                <Typography>
                    The theme type is: "{theme.palette.type}"
                </Typography>
                <HYDENFileDropzone onHYDENFilesParsed={handleHYDENFilesParsedCallback} />
            </Grid>
        </Grid>
    );
};

App.defaultProps = {};

export default App;
