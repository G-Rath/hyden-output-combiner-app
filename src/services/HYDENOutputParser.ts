import InvalidHYDENFileException from 'src/exceptions/InvalidHYDENFileException';

export interface PrimerPair {
    /**
     * What pair this pair is.
     */
    pair: string;
    /**
     * 5' primer.
     */
    primer5Inch: string;
    /**
     * 3' primer (inverted).
     */
    primer3Inch: string;
    /**
     * The coverage of this primer pair.
     */
    coverage: string;
    /**
     * The accuracy of this primer pair. (Percentage)
     */
    accuracy: string;
}

export interface PrimerParameter {
    /**
     * The name of the parameter.
     */
    name: string;
    /**
     * The value of the parameter.
     */
    value: string;
}

export interface ParsedHYDENOutputFile {
    /**
     * The number of DNA sequences that were read from the `.fas` file.
     */
    dnaSequencesRead: number;
    /**
     * The `.fas` file that the information was read from.
     */
    fasFileReadFrom: string;
    /**
     * Array of the parameters used to generate this primer.
     */
    primerParameters: Array<PrimerParameter>;
    /**
     * Collection of primer pairs that are included in this HYDEN output.
     */
    primerPairs: Array<PrimerPair>;
}

enum HYDENFileMarkers {
    FILE_HEADER_BLOCK = '================================================================================\n'
                        + '                              HYDEN v1.0 (Windows)\n'
                        + '\n'
                        + '     Copyright C. Linhart, R. Shamir and RAMOT (Tel Aviv University), 2002.\n'
                        + '================================================================================',
    PP_TABLE_HEADER_LINE = ' Pair | 5\' primer            | 3\' primer (inverted) | coverage | % (acc.)',
    PP_TABLE_BORDER_LINE = '------+----------------------+----------------------+----------+----------'
}

/**
 * Record of regexps, for matching various
 * parts of a HYDEN output file.
 * @type {{SOMETHING: RegExp}}
 */
const regexps = {
    /**
     * Matches the line in the HYDEN output that denotes the
     * number of DNA sequences read from the input file.
     *
     * Appears near the start of the file, looking like this:
     *
     * "Read 498 DNA sequences from file pseudomonas_edited_alignment_2.fas.".
     *
     * Has two capture groups - the first is the number of DNA sequences,
     * the second is the name of the `.fas` file that the sequences were read from.
     */
    READ_X_DNA_SEQUENCES_FROM_FILE_LINE: /Read (\d+) DNA sequences from file (.*?\.fas)\./,
    /**
     * Matches a line from a HYDEN output file that detail the parameters used
     * to generate the primers, parsing it
     *
     * These lines are usually at the top of the file, and look like this:
     *
     * "Dna file (fasta format) ............................................ pseudomonas_edited_alignment_2.fas"
     * "Maximal number of primer pairs to design ........................... 1"
     * "Length of 5' primer ................................................ 20"
     * "Length of 3' primer ................................................ 20"
     * "Maximal degeneracy of 5' primer .................................... 4"
     * "Maximal degeneracy of 3' primer .................................... 4"
     * "Sequence positions for 5' primer ................................... 0 to 35"
     * "Sequence positions for 3' primer ................................... -35 to -1"
     * "Maximal number of mismatches in 5' end ............................. 0"
     * "Maximal number of mismatches in 3' end ............................. 0"
     * "Maximal number of mismatches in both ends combined ................. 2"
     * "Number of DNA sequences for entropy estimation ..................... 20"
     * "Number of base primers to run contraction/expansion algorithms ..... 1000"
     * "Number of best candidates to run greedy improvement algorithm ...... 100"
     */
    PRIMER_PARAMETER_LINE: /([\w\d'() /]+) \.+ (.+)/
};

// todo: write jsdoc
class HYDENOutputParser {
    /**
     * Promises parsed HYDEN output file information, based on the contents of the given `File`.
     * @param {File} file
     *
     * @return {Promise<ParsedHYDENOutputFile>}
     *
     * @throws {InvalidHYDENFileException} when the given regexp doesn't return a match in the given string
     */
    public static async promiseParsedFile(file: File): Promise<ParsedHYDENOutputFile> {
        return this.parseFileContents(await this.promiseFileContents(file));
    }

    /**
     * Promises the contents of the given `file`.
     *
     * @param {File} file
     *
     * @return {Promise<string>>}
     */
    static async promiseFileContents(file: File): Promise<string> {
        const reader = new FileReader();

        return new Promise((resolve, reject) => {
            reader.addEventListener('error', reject);
            reader.addEventListener('load', () => {
                if (reader.result === null) {
                    return reject();
                }

                if (reader.result instanceof ArrayBuffer) {
                    throw new Error('we\'ve got an ArrayBuffer!');
                }

                resolve(reader.result);
            });

            reader.readAsText(file, 'utf-8');
        });
    }

    /**
     * Matches the given `regexp` in the given `str`,
     * or otherwise throws an `InvalidHYDENFileException`,
     * with the given `throwMessage`.
     *
     * @param {string} str
     * @param {RegExp} regexp
     * @param {string} throwMessage the string to use as the message if throwing
     *
     * @return {RegExpMatchArray}
     *
     * @throws {InvalidHYDENFileException} when the given regexp doesn't return a match in the given string
     */
    private static matchOrThrow(str: string, regexp: RegExp, throwMessage: string): RegExpMatchArray {
        const results = str.match(regexp);

        if (results === null) {
            throw new InvalidHYDENFileException(throwMessage);
        }

        return results;
    }

    /**
     * Builds a `PrimerPair` object from a `string`.
     *
     * @param {string} str
     *
     * @return {PrimerPair}
     */
    private static buildPrimerPairFromString(str: string): PrimerPair {
        const [
            pair,
            primer5Inch,
            primer3Inch,
            coverage,
            accuracy
        ] = str.split('|').map(s => s.trim());

        return {
            pair,
            primer5Inch,
            primer3Inch,
            coverage,
            accuracy
        };
    }

    /**
     * Parses the given `fileContents` as a HYDEN output file,
     * gathering the information up into a processable structure.
     *
     * @param {string} fileContents
     *
     * @return {ParsedHYDENOutputFile}
     *
     * @throws {InvalidHYDENFileException} when an error occurs while trying to parse the file contents.
     */
    public static parseFileContents(fileContents: string): ParsedHYDENOutputFile {
        // normalise line endings (in case of windows)
        const content = fileContents.replace(/\r\n/g, '\n');

        if (!content.startsWith(HYDENFileMarkers.FILE_HEADER_BLOCK)) {
            throw new InvalidHYDENFileException('File is missing starting HYDEN header');
        }

        // find the line that details the number of dna sequences that were read from the input .fas file
        const matchedReadDNASequencesFromFileLine = this.matchOrThrow(
            content,
            regexps.READ_X_DNA_SEQUENCES_FROM_FILE_LINE,
            'Unable to find "Read x DNA sequences from file x.fas." line'
        ); // while not actually needed, it's useful to make sure it's a valid file

        // the number of dna sequences read comes from the "Read X DNA sequences from file" line
        const dnaSequencesRead: number = parseInt(matchedReadDNASequencesFromFileLine[1]);
        const fasFileReadFrom: string = matchedReadDNASequencesFromFileLine[2];

        const primerPairsTable = content.substring(
            content.indexOf(HYDENFileMarkers.PP_TABLE_BORDER_LINE) + HYDENFileMarkers.PP_TABLE_BORDER_LINE.length,
            content.lastIndexOf(HYDENFileMarkers.PP_TABLE_BORDER_LINE)
        ).trim(); // removes any whitespaces, including line breaks

        return {
            dnaSequencesRead,
            fasFileReadFrom,
            primerParameters: this.parsePrimerParameters(this.matchOrThrow(
                content,
                new RegExp(regexps.PRIMER_PARAMETER_LINE, 'g'),
                'Unable to find any primer parameter lines'
            )),
            primerPairs: primerPairsTable.split('\n').map(this.buildPrimerPairFromString)
        };
    }

    /**
     * Parses an array of string primer parameters into an array of structured `PrimerParameter`s.
     *
     * @param {Array<string>} stringPrimerParameters
     *
     * @return {Array<PrimerParameter>}
     */
    private static parsePrimerParameters(stringPrimerParameters: Array<string>): Array<PrimerParameter> {
        const parsedPrimerParameters: Array<PrimerParameter> = [];

        for (const stringPrimerParameter of stringPrimerParameters) {
            try {
                const matchedPrimerParameter = this.matchOrThrow(
                    stringPrimerParameter,
                    regexps.PRIMER_PARAMETER_LINE,
                    `Unable to parse primer parameter "${stringPrimerParameter}"`
                );

                const [, name, value] = matchedPrimerParameter;

                parsedPrimerParameters.push({ name, value });

            } catch (error) {
                if (error instanceof InvalidHYDENFileException) {
                    console.warn(`Unable to parse primer parameter`, stringPrimerParameter);

                    continue;
                } // parameters are not used, so don't crash about them

                throw error;
            } // rethrow the error if it's not one of ours
        }

        return parsedPrimerParameters;
    }
}

export default HYDENOutputParser;
