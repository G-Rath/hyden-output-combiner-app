import { CssBaseline } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import DragOverPageDetector from 'src/contexts/DragOverPageDetector';
import Toastbar from 'src/components/Toastbar';
import HYDENOutputParser from 'src/services/HYDENOutputParser';
import { toast } from 'src/services/Toaster';
import theme from 'src/theme/theme';
import App from 'src/views/App';

ReactDOM.render(
    (
        <React.StrictMode>
            <DragOverPageDetector>
                <ThemeProvider theme={theme}>
                    <CssBaseline />
                    <Toastbar />
                    <App />
                </ThemeProvider>
            </DragOverPageDetector>
        </React.StrictMode>
    ),
    document.getElementById('root')
);

// @ts-ignore
window.toast = toast;
// @ts-ignore
window.parser = HYDENOutputParser;
