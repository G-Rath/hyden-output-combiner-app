import install from '@material-ui/styles/install';

/*
    this file is required until @material-ui/core v4
    is released, using the new styles system & package.

    For now, this file *must* be included before the app,
    to ensure that `install()` is called before any MUI
    components are imported.
 */
console.log('ran install');
install();
