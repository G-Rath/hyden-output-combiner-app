import { makeStyles } from '@material-ui/styles';
import React, { DragEventHandler, useCallback, useState } from 'react';

interface DragOverPageValue {
    isDraggingOverPage: boolean;
    dataTransferItems: ArrayLike<DataTransferItem>
}

export const DragOverPageDetectorContext = React.createContext<DragOverPageValue>({
    isDraggingOverPage: false,
    dataTransferItems: []
});

interface Props {
    children: React.ReactNode;
}

const useStyles = makeStyles({
    detector: {
        height: '100vh'
    }
});

// todo: todo: write jsdoc
const DragOverPageDetector = (props: Props) => {
    const classes = useStyles(props);

    const [contextValue, setContextValue] = useState<DragOverPageValue>({
        isDraggingOverPage: false,
        dataTransferItems: []
    });

    /**
     * Handles when `onDragOver` is called on a `div` element.
     *
     * @todo could save re-renders by checking if file items have changed
     *
     * @type {React.DragEventHandler<HTMLDivElement>}
     */
    const handleDragOverCallback = useCallback<DragEventHandler<HTMLDivElement>>(event => {
        setContextValue({
            isDraggingOverPage: true,
            dataTransferItems: event.dataTransfer.items
        });
    }, [setContextValue]);
    /**
     * Handles when `onDragLeave` is called on a `div` element.
     *
     * @type {React.DragEventHandler<HTMLDivElement>}
     */
    const handleDragLeaveCallback = useCallback<DragEventHandler<HTMLDivElement>>(event => {
        setContextValue({
            isDraggingOverPage: false,
            dataTransferItems: []
        });
    }, [setContextValue]);

    return (
        <DragOverPageDetectorContext.Provider value={contextValue}>
            <div
                className={classes.detector}
                onDragOver={handleDragOverCallback}
                onDragLeave={handleDragLeaveCallback}
            >
                {props.children}
            </div>
        </DragOverPageDetectorContext.Provider>
    );

};

DragOverPageDetector.defaultProps = {};

export default DragOverPageDetector;
