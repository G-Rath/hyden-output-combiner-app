# @g-rath/hyden-output-combiner-app
A lightweight browser application for combining the output files of HYDEN.

## Setup
Copy `.env.example` to `.env`, and fill out the required values.

Run `npm install`.
