process.env.NODE_ENV = 'production';
process.env.BABEL_ENV = 'production';

const merge = require('webpack-merge');

const common = require('./webpack.common');

const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = merge(common, {
    mode: 'production',
    devtool: 'source-map',
    plugins: [
        new CleanWebpackPlugin(['build'], {
            exclude: ['.gitkeep'],
            beforeEmit: true
        }),
        new CopyWebpackPlugin(
            ['public'],
            {
                ignore: [
                    'index.html'
                ]
            })
    ]
});
