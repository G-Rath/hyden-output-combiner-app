process.env.NODE_ENV = 'development';
process.env.BABEL_ENV = 'development';

const { WebpackPluginServe } = require('webpack-plugin-serve');

const common = require('./webpack.common');

const merge = require('webpack-merge');

module.exports = merge(common, {
    entry: { bundle: ['webpack-plugin-serve/client'] },
    mode: 'development',
    plugins: [
        new WebpackPluginServe({
            port: 3335,
            historyFallback: true,
            static: common.output.path,
            liveReload: true,
            host: 'localhost'
        })
    ],
    devtool: 'eval-source-map',
    devServer: {
        port: 3334,
        overlay: true,
        historyApiFallback: true
    },
    watch: true
});
