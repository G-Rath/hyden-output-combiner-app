const path = require('path');
const webpack = require('webpack');

const DotEnvPlugin = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const tsImportPluginFactory = require('ts-import-plugin');

// use BUST_CACHE if it's explicitly define; otherwise, check if production build
const cacheBust = process.env.BUST_CACHE === undefined
    ? process.env.NODE_ENV === 'production'
    : process.env.BUST_CACHE;

module.exports = {
    cache: true,
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
        alias: {
            src: path.resolve(__dirname, 'src'),
            exceptions: path.resolve(__dirname, 'src/exceptions')
        }
    },
    stats: { assetsSort: '!chunks', colors: true },
    entry: {
        bundle: [
            'src/muiInstaller.ts',
            'src/index.tsx'
        ]
    },
    output: {
        path: path.resolve('build'),
        filename: `static/js/[name].${cacheBust ? '[chunkhash]' : 'lackluster'}.js`,
        publicPath: '/'
    },
    performance: {
        maxAssetSize: 5 * 1024 * 1024,
        maxEntrypointSize: 5 * 1024 * 1024
    },
    plugins: [
        new DotEnvPlugin(),
        new HtmlWebpackPlugin({
            cache: true,
            inject: true,
            template: path.resolve('public/index.html'),
            title: 'Notebook'
        }),
        new webpack.HashedModuleIdsPlugin(),
        new webpack.ProgressPlugin((percentage, msg) => ['compiling'].includes(msg) && console.log(`${msg}...`)),
        new webpack.DefinePlugin({ 'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV) })
    ],
    optimization: {
        noEmitOnErrors: true,
        splitChunks: { chunks: 'all' }
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader', options: { importLoaders: 1 } }
                ]
            },
            {
                test: /\.tsx?$/,
                loader: 'awesome-typescript-loader',
                options: {
                    getCustomTransformers: () => ({
                        before: [
                            tsImportPluginFactory([
                                {
                                    libraryName: '@material-ui/core',
                                    libraryDirectory: '',
                                    camel2DashComponentName: false
                                },
                                {
                                    libraryName: '@material-ui/icons',
                                    libraryDirectory: '',
                                    camel2DashComponentName: false
                                }
                            ])
                        ]
                    })
                }
            },
            { test: /\.js$/, loader: 'source-map-loader', enforce: 'pre' }
        ]
    }
};
