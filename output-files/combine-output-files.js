const fs = require('fs');
const path = require('path');

/**
 * @typedef {object} HYDENOutputFile
 *
 * @property {string} outputFileName
 * @property {string} outputFileContents
 * @property {string} primerRow
 */

const TABLE_HEADER = ' Pair | 5\' primer            | 3\' primer (inverted) | coverage | % (acc.)';
const TABLE_SPACER = '------+----------------------+----------------------+----------+----------';

/**
 * The directory to read the files from.
 *
 * By default, equals `__dirname`.
 *
 * Change to another string if you want to target
 * a specific directory outside the one the file
 * is currently in.
 */
const READ_FROM_DIR = __dirname;

/**
 * Converts a HYDEN primer row into a "csv" row.
 *
 * @param {string} row
 *
 * @return {string}
 */
const convertToCSVRow = row => row.split('|').map(column => column.trim()).join(',');

/**
 * output{sequence number}_{repeat count}.txt
 */
const OUTPUT_FILE_SORTING_REGEXP = /[^\d]*(\d*)_(\d*).*/;

/**
 * Gets the numbers to use to sort the output files by.
 *
 * @param {string} outputFileName
 *
 * @return {Array<number>}
 */
const getOutputFileSortNumbers = outputFileName => {
    const results = outputFileName.match(OUTPUT_FILE_SORTING_REGEXP);

    if (results === null) {
        throw new Error(`output file ${outputFileName} doesn't follow filename convention of "output{sequence number}_{repeat count}.txt"`);
    }

    // slice the first element, as it's the original string
    return results.slice(1).map(v => parseInt(v));
};

/**
 * Gives if the file with the given `fileName` is an "output" file,
 * that should be parsed for primer data.
 *
 * @param {string} fileName
 */
const isOutputFile = fileName => fileName.startsWith('output') && fileName.endsWith('.txt');
/**
 * Sorts the given output files.
 *
 * @param outputFileNameA
 * @param outputFileNameB
 * @return {number}
 */
const sortOutputFiles = (/** string */outputFileNameA, /** string */outputFileNameB) => {
    const aNumbers = getOutputFileSortNumbers(outputFileNameA);
    const bNumbers = getOutputFileSortNumbers(outputFileNameB);

    return aNumbers // group the numbers together
        .map((value, index) => [value, bNumbers[index]])
        .reduce((result, compare) => result || compare[0] - compare[1], 0);
};

const getOutputFileContents = outputFileName => fs.readFileSync(path.join(READ_FROM_DIR, outputFileName)).toString();

/**
 * Extracts the row containing information about the
 * primer from the content of a HYDEN output file.
 *
 * @param {string} outputFileContents
 *
 * @return {string}
 */
const extractPrimerRowFromOutputFileContents = outputFileContents => outputFileContents.substring(
    outputFileContents.indexOf(TABLE_SPACER) + TABLE_SPACER.length,
    outputFileContents.lastIndexOf(TABLE_SPACER)
);

const table = fs.readdirSync(READ_FROM_DIR)
                .filter(isOutputFile)
                .sort(sortOutputFiles)
                .map(outputFileName => ({
                    outputFileName,
                    outputFileContents: getOutputFileContents(outputFileName)
                }))
                .map(outputFile => ({
                    ...outputFile,
                    primerRow: extractPrimerRowFromOutputFileContents(outputFile.outputFileContents)
                }))
                .map(/** HYDENOutputFile */row => `${row.outputFileName} | ${row.primerRow}`)
                .map(convertToCSVRow);

const csvWithHeader = [
    convertToCSVRow(`output # | ${TABLE_HEADER}`),
    ...table
].join('\r\n');

fs.writeFileSync('final.csv', csvWithHeader);

// fs.writeFileSync('outputFinal.txt', table);
